const fastify = require('fastify')({
    logger: true
});

var students = [{"StudentName": "student1",
		"StudentID": "s1",
		"suject1": 100,
		"suject2": 98,
		"suject3": 76,
		"suject4": 82,
		"suject5": 92
		},
		{
		"StudentName": "student2",
		"StudentID": "s2",
		"suject1": 92,
		"suject2": 100,
		"suject3": 98,
		"suject4": 76,
		"suject5": 82
		},
		{
		"StudentName": "student3",
		"StudentID": "s3",
		"suject1": 76,
		"suject2": 92,
		"suject3": 100,
		"suject4": 98,
		"suject5": 76
		},
		{
		"StudentName": "student4",
		"StudentID": "s4",
		"suject1": 82,
		"suject2": 76,
		"suject3": 92,
		"suject4": 100,
		"suject5": 98
		}
	]

fastify.get('/report', async (req, res) => {
    res.send(students);
});
fastify.post('/add', async (req, res) => {
    const stud  = req.body;
    students.push(stud);
    res.send(stud);
});

fastify.post('/update/:Id', async (req, res) => {
    let stud = req.body;
    let matchFound = false;

    for (let i = 0; i < students.length; i++) {
        
	 if (students[i].StudentID == req.params.Id) {
	     stud.StudentName = students[i].StudentName;
             stud.StudentID = students[i].StudentID; 
             students[i] = stud;
             matchFound = true;
             break;
        } 
    }

    if (matchFound) {
        res.send(stud); 
    } else {
        const emptyResponse = {};
        res.code(404).send(emptyResponse);
    }
});

fastify.delete('/delete/:Id', async (req, res) => {
    let stud;
    let matchFound = false;

    for (let i = 0; i < students.length; i++) {
        
	 if (students[i].StudentID == req.params.Id) {
	     stud = students[i];
	     students.splice(i, 1);
             matchFound = true;
             break;
        } 
    }

    if (matchFound) {
        res.send(stud); 
    } else {
        const emptyResponse = {};
        res.code(404).send(emptyResponse);
    }
});



const startServer = async () => { 
    try {
        await fastify.listen(2000);
    } catch (err) {
        fastify.log.error(err);
    }
};

startServer();
